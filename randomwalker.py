# Name:
# Date: 
# email: username@aims.ac.za
# Course: Programming with Python 
##################################################
#
#
#
##################################################
import matplotlib.pyplot as plt
import numpy as np
    
n =100 #number of steps
w1 = 0 #random walker 1 starting position
w2 = 0 #random walker 2 starting position
    
w1NextStep =0 
w2NextStep =0

def GetNextStep():
    """
    This function calculates the direction of the next step of 
    a random walker.
    """
    return random.randint(-1,1), random.randint(-1,1)

def GetDistanceSqr(x1):
    """
    This function calculates the random walker displacement square. 
    """
    return x1**2

def PlotDataSeparately(data1,data2,data3,data4):
    
    """
    This function produce two separate plots: one for graph of data1 and data2 the 
    other one forone for the graph data3 and data4.
    """
    
    plt.plot(zip(data1,data2))
    plt.title('Plot of positions of two independent one dimensional random walker')
    plt.ylabel('dist')
    plt.xlabel('time')
    plt.show()
        
    plt.plot(zip(data3,data4))
    plt.title('Square displacement of two independent one dimensional random walker')
    plt.ylabel('sqr dist')
    plt.xlabel('time')
    plt.show()

    """
    Here we show how to use <sublots>.
    Try to understand interpret error bars in the example. But understand how errorbar 
    function works.
    """
def PlotDataInsubplots(data1,data2,data3,data4):   
    """
    In this function the use of subplots is demonstrated.
    """
    
    x = np.arange(0, 100, 1)
    
    fig, (ax0, ax1) = plt.subplots(nrows=2, sharex=False)
    #ax0.errorbar(x,w1pos,w1pos, fmt='-o')
    ax0.plot(zip(data1,data2))
    ax0.set_title('displacement of two independent one dimensional random walker')

    #ax1.errorbar(x, w1distsqr,w2distsqr, fmt='o')
    ax1.set_title('Square displacement of two independent one dimensional random walker')
    ax1.plot(zip(data3,data4))
    ax1.set_yscale('log')
    plt.show()
    
## main =================================================================================
#if __name__ == "__main__":
#    
#    
#    
#    w1pos=[] #for storing the random walker 1 position per time step
#    w2pos=[] #for storing the random walker 1 position per time step
#    
#    w1distsqr = [] #for storing the random walker 1 square displacement per time step
#    w2distsqr = [] #for storing the random walker 2 square displacement per time step
#    
#    
#    for i in range(n):
#        
#        w1NextStep, w2NextStep = GetNextStep()
#        
#        w1 =w1 + w1NextStep
#        w2= w2 + w2NextStep
#        
#        w1pos.append(w1)
#        w2pos.append(w2)
#        
#        w1distsqr.append(GetDistanceSqr(w1))
#        w2distsqr.append(GetDistanceSqr(w2))
#    
#    PlotDataSeparately(w1pos,w2pos,w1distsqr,w2distsqr) 
#    PlotDataInsubplots(w1pos,w2pos,w1distsqr,w2distsqr) 
